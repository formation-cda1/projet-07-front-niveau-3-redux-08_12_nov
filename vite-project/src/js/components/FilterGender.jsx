import React from "react";
import "../../css/FilterGender.css";

function FilterGender({ setActiveCategory, gender, activeCategory }) {
  return (
    <div className="filtergender">
      <select
        value={activeCategory}
        onChange={(e) => setActiveCategory(e.target.value)}
        className="filtergender-select"
      >
        <option value="">Filtrer selon le genre</option>
        {gender.map((cat) => (
          <option key={cat} value={cat}>
            {cat}
          </option>
        ))}
      </select>
      <button onClick={() => setActiveCategory("")}>Réinitialiser</button>
    </div>
  );
}

export default FilterGender;
