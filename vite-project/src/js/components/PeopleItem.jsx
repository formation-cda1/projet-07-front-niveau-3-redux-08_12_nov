import React, { useState } from "react";
import "../../css/PeopleItem.css";
import { setInformationPeople } from "../features/people/peopleSlice";
import { useNavigate } from "react-router-dom";
import { useFetchSpecificPeopleQuery } from "../services/peopleApi";
import { skipToken } from "@reduxjs/toolkit/query/react";

function PeopleItem(props) {
  let navigate = useNavigate();

  const [idPeople, setIdPeople] = useState(1);
  const { name, height, mass, gender, index } = props;
  const myArr = ["Taille: " + height, "Poids: " + mass, "Genre: " + gender];

  const { data } = useFetchSpecificPeopleQuery(idPeople ?? skipToken);

  const handleClick = (id, name) => {
    console.log(id + 1);
    const num = id + 1;
    console.log(num.toString());
    setIdPeople(num.toString());

    console.log(data);
    navigate(`/starwars/people/${name}`, { redirect: true });
  };

  return (
    // <Link to={`/starwars/people/${name}`}>
    <div
      key={index}
      className="people-item"
      onClick={() => handleClick(index, name)}
    >
      <h4>{name}</h4>
      {myArr.map((item, i) => (
        <li key={item + i}>{item}</li>
      ))}
    </div>
    // </Link>
  );
}

export default PeopleItem;
