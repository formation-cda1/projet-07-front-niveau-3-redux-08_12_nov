import React from "react";
import { useSelector } from "react-redux";

export const InfoProfil = () => {
  const user = useSelector((state) => state.auth.user);

  console.log("DATA USER", user);
  return (
    <div>
      {" "}
      <h1> Prénom : {user.firstname}</h1>
      <h1> Nom : {user.lastname}</h1>
      <h1> Pseudo : {user.username}</h1>
      <h1> Email : {user.email}</h1>
    </div>
  );
};
