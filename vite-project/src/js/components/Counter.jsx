import React, { useState } from "react";
import "../../css/Counter.css";

function Counter() {
  const [count, setCount] = useState(0);
  const [isCounting, setIsCounting] = useState(false);

  const startCount = () => {
    setInterval(() => {
      setCount((count) => count + 1);
    }, 1000);

    setIsCounting(true);
  };

  const resetCount = () => setCount(0);

  return (
    <div>
      <p>Count: {count}</p>
      <div className="buttons">
        <button onClick={resetCount}>Reset</button>
        <button onClick={startCount} disabled={isCounting}>
          Start
        </button>
      </div>
    </div>
  );
}

export default Counter;
