import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { logout } from "../features/auth/authSlice";
import { Link } from "react-router-dom";

import classes from "../../css/HeaderBar.module.css";

export const HeaderBar = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.auth.user);
  const auth = useSelector((state) => state.auth);

  const handleLogout = () => {
    dispatch(logout());
    navigate("/login");
  };

  return (
    // <header style={{ display: "flex", justifyContent: "space-evenly" }}>
    //   <h2>Salut {user?.username}</h2>
    //   <button onClick={handleLogout}>logout</button>
    // </header>
    <header className={classes.header}>
      <nav>
        <ul>
          <div className={classes.itemsmenu}>
            <li>
              <NavLink
                className={(navData) =>
                  navData.isActive ? classes.active : ""
                }
                to="/home"
              >
                User Manager
              </NavLink>
            </li>
            <li>
              <NavLink
                className={(navData) =>
                  navData.isActive ? classes.active : ""
                }
                to="/starwars/people"
              >
                StarWars
              </NavLink>
            </li>
          </div>
          {auth.isAuthenticated ? (
            <li>
              <h2 style={{ color: "white" }}> Bonjour {user?.username} !</h2>
            </li>
          ) : null}
          {auth.isAuthenticated ? (
            <div className={classes.dbutton}>
              <li>
                <button onClick={() => handleLogout()}>Déconnexion</button>
              </li>
            </div>
          ) : null}
        </ul>
      </nav>
    </header>
  );
};
