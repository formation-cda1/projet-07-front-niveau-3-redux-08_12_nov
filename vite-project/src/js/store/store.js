import { configureStore } from "@reduxjs/toolkit";
import logger from "redux-logger";
// TODO: Approfondir à quoi sert redux-logger

//Services
import { peopleApi } from "../services/peopleApi";
import { authApi } from "../services/authService";
import { usersManagerApi } from "../services/usersManagerApi";
//Features
import auth from "../features/auth/authSlice";

export const store = configureStore({
  reducer: {
    [peopleApi.reducerPath]: peopleApi.reducer,
    [authApi.reducerPath]: authApi.reducer,
    [usersManagerApi.reducerPath]: usersManagerApi.reducer,
    auth,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      usersManagerApi.middleware,
      authApi.middleware,
      peopleApi.middleware,
      logger
    ), // A ajouter peopleApi.middleware
});
