import React, { useState, useRef, useEffect } from "react";
import "../../css/Login.css";
import { useRegisterMutation } from "../services/authService";
import { useLocation, useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
const Register = () => {
  let navigate = useNavigate();
  let location = useLocation();
  let from = location.state?.from?.pathname || "/";

  //body
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  //Logic
  const [formError, setFormError] = useState(null);

  //Api Logic
  const [register, { isLoading, isUpdating }] = useRegisterMutation();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const body = { firstname, lastname, password, username, email };

    //input validation
    let errorFlag = false;
    if (password.length < 6 || password.length > 15) {
      errorFlag = true;

      setFormError(
        "Password doit être d'une longueur minimale de 6 char et maximale de 15 char"
      );
    }

    try {
      const result = await register(body);
      console.log("result", result);
      if (result.error) {
        return setFormError(result.error.data.message);
      }

      navigate("/login", { redirect: true });
    } catch (err) {
      console.log("Something went wrong", err);
    }
  };

  return (
    <div className="login">
      <form className="login_form" onSubmit={(e) => handleSubmit(e)}>
        <h1>
          Pas encore inscrit ? <br /> Créer un compte et découvrrez l'univers
          Star Wars
        </h1>
        <p style={{ color: "red" }}>{formError && formError}</p>
        {isLoading && <p>Loading...</p>}
        <input
          type="text"
          name="firstname"
          placeholder="Entrez prénom"
          value={firstname}
          onChange={(e) => setFirstname(e.target.value)}
        />
        <input
          type="text"
          name="lastname"
          placeholder="Entrez votre nom"
          value={lastname}
          autoComplete="on"
          onChange={(e) => setLastname(e.target.value)}
        />
        <input
          type="text"
          name="username"
          placeholder="Entrez votre pseudo"
          value={username}
          autoComplete="on"
          onChange={(e) => setUsername(e.target.value)}
        />
        <input
          type="email"
          name="email"
          placeholder="Entrez votre email"
          value={email}
          autoComplete="on"
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          type="password"
          name="password"
          placeholder="Entrez mot de passe"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <button type="submit" className="submit_btn">
          S'enregistrer
        </button>
        <div className="link">
          <Link to="/login">Déjà inscrit ? Connectez vous ici</Link>
        </div>
      </form>
    </div>
  );
};

export default Register;
