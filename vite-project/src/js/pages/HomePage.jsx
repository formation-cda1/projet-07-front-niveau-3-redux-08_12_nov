import React, { useState } from "react";
import { Link, Outlet } from "react-router-dom";
import Counter from "../components/Counter";
import "../../css/Home.css";
function HomePage() {
  // const handleClick = async (e) => {
  //   e.preventDefault();
  //   window.location.reload();
  // };
  // console.log("DATA", data);
  const [displayTimer, setdisplayTimer] = useState(false);
  const [hideOrDisplay, sethideOrDisplay] = useState("Afficher");
  const displayButton = () => {
    if (displayTimer === false) {
      sethideOrDisplay("Cacher");
      setdisplayTimer(true);
    } else {
      sethideOrDisplay("Afficher");
      setdisplayTimer(false);
    }
  };
  return (
    <section>
      {displayTimer ? <Counter /> : null}
      <button onClick={() => displayButton()}>{hideOrDisplay} le Timer</button>
      {/* <h1>Vous êtes bien connecté</h1> */}
      <div className="all-btn-link">
        <Link className="link-btn" to="users">
          Afficher la liste des utilisateurs
        </Link>
        <Link className="link-btn" to="research-user">
          Afficher la page de recherche des utilisateurs
        </Link>
        <Link className="link-btn" to="profil">
          Afficher le profil
        </Link>
      </div>
      <Outlet />
    </section>
  );
}

export default HomePage;
