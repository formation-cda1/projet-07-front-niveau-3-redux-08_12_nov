import React, { useState } from "react";
import {
  useFetchAllPeopleQuery,
  useFetchPeopleQuery,
} from "../services/peopleApi";
import { useDispatch, useSelector } from "react-redux";
import FilterGender from "../components/FilterGender";
import PeopleItem from "../components/PeopleItem";
import "../../css/ListPeople.css";

export const PeopleStarWars = () => {
  const [pageMax, setPageMax] = useState(0);
  const [page, setPage] = useState(1);
  const { data: peopleStarwars, isFetching } = useFetchAllPeopleQuery(page);
  //   const { data } = useFetchPeopleQuery();
  //   console.log(data);
  const numberOfPeople = peopleStarwars?.count;
  //   setPageMax(Math.ceil(numberOfPeople / 10));
  //
  const [searchTerm, setSearchTerm] = useState("");
  const [activeCategory, setActiveCategory] = useState("");

  const dispatch = useDispatch();

  if (isFetching) return <div>People loading ...</div>;

  const gender = peopleStarwars?.results?.reduce(
    (acc, people) =>
      acc.includes(people.gender) ? acc : acc.concat(people.gender),
    []
  );

  function detailsPeople(e) {
    e.preventDefault();
    // dispatch(setInformationPeople());
    // navgate("/login");
  }

  //   function loadAllPeople() {
  //     const numberOfPeople = data?.count;
  //     const pages = [];
  //     for (let i = 1; i <= Math.ceil(numberOfPeople / itemsPerPage); i++) {
  //       const { data: dataPerPage } = useFetchAllPeopleQuery(i);
  //       console.log(dataPerPage);
  //       pages.push(dataPerPage);
  //     }
  //   }

  return (
    <div>
      <div className="layout-search-btn">
        <input
          className="search-bar"
          type="text"
          placeholder="Recherche un personnage par son nom"
          onChange={(event) => {
            setSearchTerm(event.target.value);
          }}
        />
        {/* <button>Ok</button> */}
      </div>
      <div className="people-filter-list">
        <FilterGender
          gender={gender}
          setActiveCategory={setActiveCategory}
          activeCategory={activeCategory}
        />

        <ul className="people-list">
          {peopleStarwars?.results
            ?.filter((val) => {
              if (searchTerm == "") {
                return val;
              } else if (
                val.name.toLowerCase().includes(searchTerm.toLowerCase())
              ) {
                return val;
              }
            })
            .map(({ name, height, mass, gender }, index) =>
              !activeCategory || activeCategory === gender ? (
                <div key={name}>
                  <PeopleItem
                    index={index}
                    name={name}
                    height={height}
                    mass={mass}
                    gender={gender}
                  />
                  <button onClick={(e) => detailsPeople(e)}>
                    Voir détails
                  </button>
                </div>
              ) : null
            )}
        </ul>
      </div>
      <div className="btn-paging">
        <button
          className="btn-disabled"
          onClick={() => setPage(page - 1)}
          disabled={page == 1}
        >
          Page prédécente
        </button>
        <button onClick={() => setPage(page + 1)}>Page suivante</button>
      </div>
    </div>
  );
};
