import { createSlice } from "@reduxjs/toolkit";
import { peopleApi } from "../../services/peopleApi";

const initialState = {
  peopleInfo: null,
  isLoading: false,
};

export const peopleSlice = createSlice({
  name: "peopleStarWars",
  initialState,
  reducers: {
    setInformationPeople: (state, action) => {
      console.log("setInformationPeople", action);
      state.peopleInfo = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setInformationPeople } = peopleSlice.actions;

export default peopleSlice.reducer;
