import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const peopleApi = createApi({
  reducerPath: "peopleApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://swapi.dev/api/",
  }),
  endpoints(builder) {
    return {
      fetchPeople: builder.query({
        query() {
          return `/people/`;
        },
      }),
      fetchAllPeople: builder.query({
        query: (pageNumber) => ({
          url: `/people/?page=${pageNumber}`,
        }),
      }),
      fetchSpecificPeople: builder.query({
        query: (peopleId) => ({
          url: `/people/${peopleId}/`,
        }),
      }),
    };
  },
});

export const {
  useFetchPeopleQuery,
  useFetchAllPeopleQuery,
  useFetchSpecificPeopleQuery,
} = peopleApi;
