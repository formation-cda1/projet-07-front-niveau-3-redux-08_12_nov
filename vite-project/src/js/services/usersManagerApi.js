import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const API_BASE_URL = "https://api.pote.dev";

export const usersManagerApi = createApi({
  reducerPath: "usersManagerApi",
  baseQuery: fetchBaseQuery({
    baseUrl: API_BASE_URL,
  }),
  endpoints(builder) {
    return {
      fetchListUsers: builder.query({
        query() {
          return `/users/public`;
        },
      }),
    };
  },
});

export const { usefetchListUsersQuery } = usersManagerApi;
