import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const API_BASE_URL = "https://api.pote.dev";

const baseQuery = fetchBaseQuery({
  baseUrl: API_BASE_URL,
  prepareHeaders: (headers, { getState }) => {
    const accessToken = getState().auth.accessToken;
    const xsrfToken = getState().auth.xsrfToken;
    // If we have a token set in state, let's assume that we should be passing it.
    // console.log("access Token", accessToken);
    // console.log("xsrfToken", xsrfToken);

    if (accessToken && xsrfToken) {
      headers.set("authorization", `Bearer ${accessToken}`);
      headers.set("x-xsrf-token", xsrfToken);
    }

    return headers;
  },
});

const loginRequest = (endpoint, body) => ({
  url: endpoint,
  method: "POST",
  body: body,
});
const authMeRequest = (endpoint) => ({
  url: endpoint,
  method: "GET",
});

const authTokenRequest = (endpoint, body) => ({
  url: endpoint,
  method: "POST",
  body: body,
});

export const authApi = createApi({
  reducerPath: "authApi",
  baseQuery: baseQuery,
  keepUnusedDataFor: false,
  endpoints: (builder) => ({
    login: builder.mutation({
      query: (userInfo) => loginRequest("/auth/login", userInfo),
    }),
    fetchCurrentUser: builder.query({
      query: () => authMeRequest("/auth/me"),
    }),
    authToken: builder.mutation({
      query: (emailToken) => authTokenRequest("/auth/token", emailToken),
    }),
    register: builder.mutation({
      query: (body) => ({
        url: "/users/",
        method: "POST",
        body,
      }),
    }),
  }),
});

export const {
  useLoginMutation,
  useFetchCurrentUserQuery,
  useAuthTokenMutation,
  useRegisterMutation,
} = authApi;
