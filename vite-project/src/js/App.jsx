import React, { useState } from "react";
import { Routes, Route, Navigate } from "react-router-dom";

//auth
import { RequireAuth } from "./features/auth/requireAuth";
import { useIsAuth } from "./hooks/useIsAuth";

//Public
import Login from "./pages/Login";
import Register from "./pages/Register";
import { HeaderBar } from "./components/HeaderBar";
// import { UsersManager } from './features/users/usersManager';
// import { RegisterPage } from './features/register/RegisterPage';

//Protected
import HomePage from "./pages/HomePage";
import { ListUsers } from "./components/ListUsers";
import { PeopleStarWars } from "./pages/PeopleStarWars";
import PeopleStarWarsDetails from "./pages/PeopleStarWarsDetails";
// import { StartshipsManager } from './features/starships/starshipsManageR';

import "../css/App.css";
import { InfoProfil } from "./components/InfoProfil";

function App() {
  useIsAuth();
  // console.log("IS FETCHING USER", isFetching);

  // if (isFetching) return <p>App is Loading</p>;

  const displayHeader = () => {
    if (
      window.location.pathname !== "/login" &&
      window.location.pathname !== "/register"
    ) {
      return <HeaderBar />;
    } else {
      return null;
    }
  };

  return (
    <div>
      {/* {displayHeader()} */}
      <HeaderBar />
      <main>
        <Routes>
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/" element={<Navigate replace to="/home" />} />
          <Route
            path="/home"
            element={
              // <RequireAuth>
                <HomePage />
              // </RequireAuth>
            }
          >
            <Route path="users" element={<ListUsers />}></Route>
            <Route
              path="research-user"
              element={<p>Rechercher des utilisateurs</p>}
            ></Route>
            <Route path="profil" element={<InfoProfil />}></Route>
          </Route>
          <Route
            path="/starwars/people"
            element={
              // <RequireAuth>
                <PeopleStarWars />
              // </RequireAuth>
            }
          />
          <Route
            path="/starwars/people/:peopleId"
            element={
              // <RequireAuth>
                <PeopleStarWarsDetails />
              // </RequireAuth>
            }
          />
          {/* <Route
        path="/people"
        element={
          <RequireAuth>
            <PeopleManager />
          </RequireAuth>
        }
      /> */}
          {/* <Route
        path="/starships"
        element={
          <RequireAuth>
            <StartshipsManager />
          </RequireAuth>
        }
      />*/}
        </Routes>
      </main>
    </div>
  );
}

export default App;
