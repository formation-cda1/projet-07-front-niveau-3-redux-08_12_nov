import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  useFetchCurrentUserQuery,
  useAuthTokenMutation,
} from "../services/authService";

import { setCredentials } from "../features/auth/authSlice";
import { getLocalStorageItem } from "../utils/localstorage";
import { setIsInitialized } from "../features/auth/authSlice";

export const useIsAuth = () => {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const [authToken, { isLoading, isUpdating }] = useAuthTokenMutation();

  useEffect(() => {
    const accessToken = getLocalStorageItem("accessToken");
    const xsrfToken = getLocalStorageItem("xsrfToken");

    if (accessToken && xsrfToken) {
      dispatch(setCredentials({ accessToken, xsrfToken }));
    } else {
      dispatch(setIsInitialized());
    }

    const refreshToken = getLocalStorageItem("refreshToken");
    const email = getLocalStorageItem("email");
    const body = { email, refreshToken };
    console.log("body", body);
    if (email && refreshToken) {
      async () => {
        const result = authToken(body);
        console.log("result auth/token", result);
      };
    }
  }, []);

  const { error, refetch } = useFetchCurrentUserQuery();

  //Todo remove cach from get /auth/me
  if (auth.isAuthenticated && !auth.user) {
    refetch();
  }

  if (error) {
    console.log("error in fetching user");
    dispatch(setIsInitialized());
  }
};
