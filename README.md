# Projet-07-Front Niveau 3 Redux-08_12_Nov

Lien vers le projet sur Notion CPROM https://www.notion.so/Front-Niveau-3-Redux-3cc7447687594b708f4cac509da1cff6


## Maquettes/ Wireframes Figma UX-UI

Ci dessous sont présenté les wireframes du projet. Les différents écrans sont cliquables , vous pouvez donc naviguer à travers les écrans en cliquant sur les différents boutons. 

Lien vers Wireframe/Maquette Figma [Cliquez ici](https://www.figma.com/proto/z0CzROOE5tx9akMxjjrPk5/BROUILLON?node-id=306%3A786&scaling=contain&page-id=0%3A1&starting-point-node-id=306%3A786&show-proto-sidebar=1)

